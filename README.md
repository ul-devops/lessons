# Lessons

`Lessons` is the git repository holding the content material for the UL Devops courses @ Polytech Nancy.

# Table of content

* [Session 1][session-1]
* [Session 2][session-2]
* [Session 3][session-3]
* [Session 4][session-4]

[session-1]: ./part-1/session-1.md
[session-2]: ./part-1/session-2.md
[session-3]: ./part-1/session-3.md
[session-4]: ./part-2/session-4.md
