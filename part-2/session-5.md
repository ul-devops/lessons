# Session 5

This session is the last of the part-2 section. We will remember what we have seen previous part and talk about observability.

## What did we learn last time?

## Observability

With complex infrastructure, we need to get metrics about what's running, memory, error count, etc. This can be done with two powerful technos: Prometheus & Grafana (as usual, these are examples: you can explore other alternatives)

## Hands-on Prometheus & Grafana

1. Install `helm`, it's a Kubernetes package manager (to easily deploy complex applications on a Kubernetes cluster): https://helm.sh/docs/intro/install/#from-the-binary-releases

2. Verify that it works correctly:
```
$ helm version
version.BuildInfo{Version:"v3.12.3", GitCommit:"3a31588ad33fe3b89af5a2a54ee1d25bfe6eaa5e", GitTreeState:"clean", GoVersion:"go1.20.7"}
```

3. Deploy Prometheus & Grafana:
```
$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
$ helm install prometheus-01 prometheus-community/kube-prometheus-stack
```

Congratulations, in three commands you setup and configured a whole observability stack. Let's now explore it, you can access it with `port-forward`
```
$ kubectl port-forward svc/prometheus-01-grafana 8080:80
```

Grafana portal should be reachable on http://localhost:8080. Username is `admin` and password is `prom-operator`

