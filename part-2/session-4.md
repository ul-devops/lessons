# Session 4

This session is the first of the part-2 section. We will remember what we have seen in part-1 and talk about container orchestration.

## What did we learn last time?

## Container orchestration
* reminder on containers
* do you run containers at work?
* container runtime
* how would you deploy a container?
* why using containers?
* https://cloudplatform.googleblog.com/2014/06/an-update-on-container-support-on-google-cloud-platform.html (2 billion container / week instances in 2014)
* Kubernetes

## Hands-on Docker

Let's learn how to create a simple application with Docker.

1. Install Docker on your Linux: 
```
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

2. Check that it works

```
sudo docker run hello-world
```

3. Create your first image with a Dockerfile, create a directory called `my-image` then add the following content in a `Dockerfile` file

```
FROM nginx
COPY my-site /usr/share/nginx/html
```

Create `my-site` directory and add the following to `index.html`:
```
Hello from the container :D 
```

4. Build the image:
```
sudo docker build -t mon-server .
```

NOTE: Do not forget the `.`

5. Run the image:
```
sudo docker run --rm -ti -p 8080:80 mon-server
```

6. Go on your browser to http://localhost:8080/

## Hands-on Kubernetes

In this hands-on, you will learn how to deploy a simple application on a local Kubernetes cluster using [KinD](https://kind.sigs.k8s.io/) (Kubernetes in Docker).

NOTE: You might need to install Docker if it's not available yet.

1. Install KinD binary (it will help you to quickly create a local Kubernetes cluster in Docker)
```bash
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.26.0/kind-linux-amd64
chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind
```

2. Create a simple Kubernetes cluster
```bash
kind create cluster
```

3. Install [`kubectl`](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/), it's the CLI used to run commands against a Kubernetes cluster
```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

4. Check that you Kubernetes cluster is correctly deployed
```
kubectl get nodes
```

5. Now deploy your first application, let's keep simple with a webserver:
```
wget https://k8s.io/examples/application/deployment-update.yaml
# read ./deployment-update.yaml
kubectl apply -f ./deployment-update.yaml
kubectl get deployments
# wait for :
# NAME               READY   UP-TO-DATE   AVAILABLE   AGE
# nginx-deployment   2/2     2            2           25s
```

NOTE: You can use `-w/--watch` for 'watching' the command line output.

QUESTION: Every Kubernetes components can be described with YAML configuration language. What are the pros?

6. That would be interesting to 'expose' the deployment to the world using a [service](https://kubernetes.io/docs/concepts/services-networking/service/), to be sure that our application is working :)

Let's update our `deployment-update.yaml` with the following:
```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: nginx
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx
  type: ClusterIP
```

NOTE: We can see things in this way: internet -> service -> deployments -> pods -> containers

Apply again the manifest: `kubectl apply -f ./deployment-update.yaml`

7. You can open a browser, or a curl command to check that your Nginx is up and running:
```
kubectl port-forward service/nginx 8080:80
curl http://localhost:8080
```

8. What if you delete one pod? Two of them? https://kubernetes.io/docs/reference/kubectl/cheatsheet/#deleting-resources

---

9. Let's voluntary break the system - update the `update-deployment.yml` file and change the `nginx` tag to `toto`, then apply. What happens? Is your service available?

9.1. Check your pods, and describe the one who's failing (`kubectl describe pod/nginx-deployment-oieeofb`). Why does it fail?

9.2 Check the deployment history, find which one is faulty and save your cluster:
- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#rollout (history)
- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#rollout (undo)


### Conclusion

![overview.png](./overview.png)

### Project ideas

#### Technical
* Deploy your own Kubernetes cluster (without using KinD nor Minikube) with `kubeadm`
* Follow a tutorial on Flatcar: a Linux based OS designed to run containers/Kubernetes only (https://www.flatcar.org/docs/latest/tutorial/)
* Deploy an application on a Kubernetes cluster using CI/CD

#### High level projects
* Compare Kubernetes with other tools (Docker Swarm for example)
* Try to define a "CI/CD" pipeline: from `git push` to the production using the concepts/techno that we have seen
* Cluster API
